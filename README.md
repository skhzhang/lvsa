# LvsA

Lasers vs. Asteroids is a game based on Margaret Lamb's simple game where squares simply bounce within a frame. It was my submission for assignment #7 of CISC 124.

See the assignment requirements here: http://research.cs.queensu.ca/home/cisc124/2014w/assignments/assn7/assn7.html

Additional changes I made to the game (not required):
- added a dark-blue background with stars that adjust position depending on the panel dimensions
- shapes move at different speeds
- bottom panel indicates the number of points each object gives (uses panels to indicate each colour)
- confirmation dialog when the player hits the stop button
- a laserbeam appears to "destroy" a box/object when the box/object is clicked
- an explosion-type effect appears when a box/object is clicked
- game over dialog pops-up when the screen is full or when stop is clicked; this displays the player's name and finishing score

Minor changes (also not required):
- colours have been changed from: green->brown, blue->gray, red-> blue, and gray->orange to fit a "space" theme
- changed the colour of the border to black for aesthetic purposes
- changed the name of the game from "Moving Squares" to "Lasers and Asteroids"
- changed the creation interval to 2 seconds
