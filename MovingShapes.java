package assignment7;
/**
 * A game called "Lasers and Asteroids" that essentially involves the user clicking on moving
 * boxes and it keeps score. 
 * 
 * To be used with the class 'Square'.
 * 
 * CISC 124, winter 2014
 * 
 * Originally by M. Lamb, modified by Simon Zhang
 * 
 * Simon Zhang 
 */
/* Original header comment:
 * This is a slightly modified version of the GUI program I supplied for Assignment 4.  The only changes
 * are:
 *     calls the "paint" method from the Square class
 *     puts a border around the inner panel so the squares
 * 
 * For Assignment 7, you must make changes and additions to this class and the Square class.  Please read
 * the web page for this assignment carefully for instructions about the changes you must make and the
 * restrictions about things you can't change.
 * 
 * CISC 124, Winter 2014
 * M. Lamb
 */

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import java.util.ArrayList;

public class MovingShapes extends JFrame implements ActionListener {

	// A timer to "tick" every 20 milliseconds (or as close to that as the system can manage).
	// If you want the program to run slower while you're debugging, just increase the 20 to
	// a larger number.
	private Timer timer = new Timer(20, this);

	// Number of ticks between creation of new squares. If you want fewer or more shapes,
	// you can change this number.
	private static final int CREATION_INTERVAL = 200; // 2 seconds

	// A list of the squares showing in the window. We'll be talking about the ArrayList class soon;
	// it's like a Python list.
	private ArrayList<Square> squareList = new ArrayList<Square>();

	// The inner area of the window -- the part that can contain squares (doesn't include
	// title, menu bar, and borders)
	private MovingSquarePanel innerPanel = new MovingSquarePanel();

	// Initial dimensions of the inner panel. The user can change the size of the frame while
	// the program is running.
	private static final int INITIAL_PANEL_WIDTH = 600;
	private static final int INITIAL_PANEL_HEIGHT = 400;

	// Count of number of ticks until it's time to create a new square
	private int creationCountdown = 0;

	// initial size for squares
	private int INITIAL_SQUARE_SIZE = 20;
	// Number of pixels a square grows after each collision
	final int SIZE_INCREMENT = 10;

	// Sequence of colors for squares -- they start at the first color
	// and move to the next after each collision until they reach the last
	private static Color asteroid = new Color(56, 42, 0);
	private static final Color squareColors[] = { asteroid, Color.GRAY,
			Color.BLUE, Color.ORANGE, Color.BLACK };
	private static final Color INITIAL_COLOR = squareColors[0];
	private static final Color LAST_COLOR = squareColors[squareColors.length - 1];

	// Pointer to the main frame of the program (for referencing from inside inner classes)
	private JFrame thisFrame = this;

	// Additional variables:
	private JPanel topLeft;
	private JLabel nameLabel;
	private JTextField nameField;
	private JButton nameConfirm;
	private JLabel scoreLabel;
	private int scoreTotal = 0;
	private JTextField scoreField = new JTextField(scoreTotal + " pts", 4);
	private JButton stopButton;
	private JButton pauseButton;
	private boolean paused = false;

	private boolean hit = false;
	private int laserPosY = Square.getPanelHeight() - 25;
	private int randomColor = (int) (Math.random() * 4);
	private int debrisPos = 0;

	// Constructor to set up the window for the program
	public MovingShapes() {
		// set position of window in screen (better for demo videos)
		setLocation(new Point(600, 100));

		setTitle("Lasers and Asteroids");
		// Make sure program cleans itself up when the user closes the window
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		// Make the inner panel part of the window and specify its initial size
		add(innerPanel);

		// Fonts for the labels in the GUI
		Font titleFont = new Font("Sanserif", Font.BOLD, 16);
		Font titleFont2 = new Font("Sanserif", Font.PLAIN, 16);

		// bottom panel, displays the amount of points for each box/object
		JPanel bot = new JPanel(new FlowLayout(FlowLayout.CENTER, 15, 12));
		bot.setBackground(Color.BLUE);
		bot.setPreferredSize(new Dimension(INITIAL_PANEL_WIDTH, 50));
		Color background = new Color(0, 20, 82);
		bot.setBackground(background);
		add(bot, BorderLayout.SOUTH);

		JLabel bot1 = new JLabel("Points: ");
		bot1.setFont(titleFont);
		bot1.setForeground(Color.WHITE);
		bot.add(bot1);

		JPanel bot2 = new JPanel();
		bot2.setBackground(asteroid);
		bot.add(bot2);

		JLabel bot2Text = new JLabel("asteroid: 1 pt");
		bot2Text.setFont(titleFont2);
		bot2Text.setForeground(Color.WHITE);
		bot2.add(bot2Text);

		JPanel bot3 = new JPanel();
		bot3.setBackground(Color.GRAY);
		bot.add(bot3);

		JLabel bot3Text = new JLabel("moon: 2 pts");
		bot3Text.setFont(titleFont2);
		bot3Text.setForeground(Color.WHITE);
		bot3.add(bot3Text);

		JPanel bot4 = new JPanel();
		bot4.setBackground(Color.BLUE);
		bot.add(bot4);

		JLabel bot4Text = new JLabel("comet: 3 pts ");
		bot4Text.setFont(titleFont2);
		bot4Text.setForeground(Color.WHITE);
		bot4.add(bot4Text);

		JPanel bot5 = new JPanel();
		bot5.setBackground(Color.ORANGE);
		bot.add(bot5);

		JLabel bot5Text = new JLabel("star: 4 pts ");
		bot5Text.setFont(titleFont2);
		bot5Text.setForeground(Color.BLACK);
		bot5.add(bot5Text);

		JPanel bot6 = new JPanel();
		bot6.setBackground(Color.BLACK);
		bot.add(bot6);

		JLabel bot6Text = new JLabel("black hole: -1 pt");
		bot6Text.setFont(titleFont2);
		bot6Text.setForeground(Color.WHITE);
		bot6.add(bot6Text);

		// top panel
		JPanel top = new JPanel(new GridLayout(1, 3, 15, 12));
		top.setBackground(background);
		add(top, BorderLayout.NORTH);

		topLeft = new JPanel(new FlowLayout(FlowLayout.LEADING, 5, 20));
		FlowLayout topLeftLayout = new FlowLayout();
		topLeftLayout.setAlignment(FlowLayout.LEADING);
		topLeft.setLayout(topLeftLayout);
		topLeft.setBackground(background);
		top.add(topLeft);

		// Labels the name field so that the user knows where to put their name
		nameLabel = new JLabel("Pilot: ");
		nameLabel.setFont(titleFont);
		nameLabel.setForeground(Color.WHITE);
		topLeft.add(nameLabel);

		// Adds a name field that allows the user to enter their name
		nameField = new JTextField("", 10);
		nameField.setEditable(true);
		nameField.addActionListener(this);
		topLeft.add(nameField);

		// OK button for the name field
		nameConfirm = new JButton("OK");
		nameConfirm.addActionListener(this);
		topLeft.add(nameConfirm);

		JPanel topMid = new JPanel(new FlowLayout(FlowLayout.CENTER, 5, 10));
		topMid.setBackground(background);
		top.add(topMid);

		// Labels the score field
		scoreLabel = new JLabel("Current Score: ");
		scoreLabel.setFont(titleFont2);
		scoreLabel.setForeground(Color.WHITE);
		topMid.add(scoreLabel);

		// Keeps score
		scoreField.setHorizontalAlignment(JTextField.RIGHT);
		topMid.add(scoreField);
		scoreField.setEditable(false);

		JPanel topRight = new JPanel(new FlowLayout());
		FlowLayout topRightLayout = new FlowLayout();
		topRightLayout.setAlignment(FlowLayout.TRAILING);
		topRight.setLayout(topRightLayout);
		topRight.setBackground(background);
		top.add(topRight);

		// Adds a pause button that allows the user to pause/resume the game
		pauseButton = new JButton("pause");
		pauseButton.addActionListener(this);
		topRight.add(pauseButton);

		// Adds a stop button that allows the user to end the game
		stopButton = new JButton("stop");
		stopButton.setForeground(Color.RED);
		stopButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//confirmation dialog box appears when stop has been pressed
				int answer = JOptionPane.showConfirmDialog(innerPanel,
						"Do you want to end the game?", "End Game",
						JOptionPane.YES_NO_OPTION);
				
				if (answer == JOptionPane.YES_OPTION) {
					//stop the timer, display name and score in another dialog box
					timer.stop();
					if (!nameField.getText().equals("")) {
						JOptionPane.showMessageDialog(thisFrame, "GAME OVER!\n"
								+ nameField.getText() + "'s score is "
								+ scoreTotal, "Game Over",
								JOptionPane.WARNING_MESSAGE);
					} else { //alternative, do not display name if none has been given
						JOptionPane.showMessageDialog(thisFrame,
								"GAME OVER!\nYour score is " + scoreTotal,
								"Game Over", JOptionPane.WARNING_MESSAGE);
					}
					System.exit(0);
				}
			}
		});
		topRight.add(stopButton);

		// Set the background to a dark blue colour
		Color space = new Color(0, 14, 56);
		innerPanel.setBackground(space);

		// Add a "listener" to react every time the size of the window is changed.
		innerPanel.addComponentListener(new Resizer());
		innerPanel.setPreferredSize(new Dimension(INITIAL_PANEL_WIDTH,
				INITIAL_PANEL_HEIGHT));

		// Add a border to the panel so it looks better when shapes bounce off the edge
		innerPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));

		// Specify that the actionPerformed method will be called each time the timer ticks
		timer.addActionListener(this);

		// Add the mouse listener to figure out if a box/object has been clicked.
		innerPanel.addMouseListener(new Listener());

		// Now that everything's set up, show the window on the screen and start the timer
		pack();
		setVisible(true);
		timer.start();

	} // end MovingShapes

	// This method is called each time the timer "ticks". It updates the position of every
	// square, creates a new square if it's time, and handles collisions.
	// Also handles the GUI buttons and fields in the top panel.
	public void actionPerformed(ActionEvent e) {
		Object source = e.getSource();

		if (source == timer) {

			/***** Make each square move. *****/
			// The square itself will know what direction it needs to move and what to do if it
			// hits the boundary of the panel. All we need to do here is tell it to move.
			for (Square square : squareList)
				square.move();

			// Search for collisions. When a pair of collide, the newer one disappears and the
			// older one grows and changes color (unless it's already black)
			for (int i = 0; i < squareList.size(); i++) {
				Square squareA = squareList.get(i);
				for (int j = i + 1; j < squareList.size(); j++) {
					Square squareB = squareList.get(j);
					/* see if the two squares are have collided */
					if (Square.overlap(squareA, squareB)) {
						// Get rid of squareB
						squareList.remove(j);
						j--; // so that we won't skip checking the next square
						// squareA grows & changes to next color unless it's already at the last
						// color
						// (black)
						squareA.grow(SIZE_INCREMENT);
						Color colorA = squareA.getColor();
						boolean found = false;
						for (int colorIndex = 0; !found
								&& colorIndex < squareColors.length - 1; colorIndex++) {
							if (colorA == squareColors[colorIndex]) {
								colorA = squareColors[colorIndex + 1];
								squareA.setColor(colorA);
								found = true;
							} // end if
						} // end for
						if (colorA == LAST_COLOR)
							squareA.stop();
					} // end if
				} // end for

			} // end for

			// If it's time to create a new square, do that, but make sure it doesn't appear
			// on top of an existing square. If the screen is so full that this can't be done
			// after the maximum number of tries, the program ends.
			final int MAX_TRIES = 100; // number of times we try to create a new square before
										// giving up
			if (creationCountdown == 0) {
				int tries = 0; // number of times we try to create a new square in a place
				boolean success = false; // becomes true when we've successfully created a new
											// square
											// that doesn't overlap with an older one
				Square newSquare = null;
				while (!success && tries < MAX_TRIES) {
					newSquare = new Square(INITIAL_SQUARE_SIZE, INITIAL_COLOR); // square
																				// constructor
																				// picks a random
																				// direction and
																				// position
					// See if the new square overlaps any of the others
					boolean hasOverlap = false; // true if the new square overlaps with an existing
												// one
					for (int i = 0; i < squareList.size() && !hasOverlap; i++) {
						if (Square.overlap(squareList.get(i), newSquare)) {
							hasOverlap = true;
						} // end if
					} // end for
					if (!hasOverlap) {
						success = true;
						break;
					} else {
						tries++;
					} // end if
				} // end while
				if (success) {
					squareList.add(newSquare);
					creationCountdown = CREATION_INTERVAL; // re-start count until time to add
															// another
															// shape
				} else {
					// Could not create a new square without overlapping with another: end program.
					timer.stop();
					if (!nameField.getText().equals("")) {
						JOptionPane.showMessageDialog(
								thisFrame,
								"SCREEN IS TOO FULL! GAME OVER!\n"
										+ nameField.getText() + "'s score is "
										+ scoreTotal, "Game Over",
								JOptionPane.WARNING_MESSAGE);
					} else {
						JOptionPane.showMessageDialog(thisFrame,
								"SCREEN IS TOO FULL! GAME OVER!\nYour score is "
										+ scoreTotal, "Game Over",
								JOptionPane.WARNING_MESSAGE);
					}

					System.exit(0);
				} // end if
			} else {
				creationCountdown--;
			} // end if

			// Tell the inner panel to re-display its contents according to the
			// updated list of squares
			innerPanel.repaint();

		} else if (source == nameConfirm || source == nameField) {
			// when enter is pressed in the name field, or if the OK button is clicked:
			// adds the user's name to the score label and the name label, and deletes the name
			// field and its confirm button
			if (!nameField.getText().equals("")) { // only if the name field is not empty

				scoreLabel.setText(nameField.getText().trim()
						+ "'s Current Score: ");
				nameLabel.setText("Pilot: " + nameField.getText().trim());
				// nameField.setEditable(false);
				// nameConfirm.setEnabled(false);
				topLeft.remove(nameField);
				topLeft.remove(nameConfirm);
				topLeft.repaint();
			} else
				scoreLabel.setText("Current Score: ");

		} else if (source == pauseButton && paused == false) {
			// when the pause button is clicked,
			// stop the timer and change to a resume button
			timer.stop();
			pauseButton.setText("resume");
			paused = true;

		} else if (source == pauseButton && paused == true) {
			// when the resume button is clicked,
			// start the game up again and change to a pause button
			timer.start();
			pauseButton.setText("pause");
			paused = false;
		}
	} // end actionPerformed

	// This is an inner class that specifies what should happen if the window is resized.
	// Its componentResized method will be called at the start of the program and then
	// each time the user re-sizes the window.
	// It's responsible for informing the square class of its new limits.
	private class Resizer extends ComponentAdapter {
		public void componentResized(ComponentEvent e) {
			// Get the new dimensions of the inner panel
			int panelWidth = innerPanel.getWidth();
			int panelHeight = innerPanel.getHeight();

			// Tell the Square class that the size of its enclosing panel has changed
			Square.setPanelDimensions(panelWidth, panelHeight);

		} // end componentResized
	} // end Resizer

	// This is an inner class for the inner panel. It adds knowledge about how to "paint" the
	// contents of the panel to the standard JPanel class
	private class MovingSquarePanel extends JPanel {
		// This method describes how to "paint" the squares inside the panel
		public void paintComponent(Graphics gc) {
			super.paintComponent(gc); // default panel drawing

			// Paint (lots of) stars in the background
			gc.setColor(Color.YELLOW);
			gc.fillOval(Square.getPanelWidth() * 100 / 600,
					Square.getPanelHeight() * 50 / 400, 5, 5);
			gc.fillOval(Square.getPanelWidth() * 40 / 600,
					Square.getPanelHeight() * 200 / 400, 5, 5);
			gc.fillOval(Square.getPanelWidth() * 100 / 600,
					Square.getPanelHeight() * 325 / 400, 5, 5);
			gc.fillOval(Square.getPanelWidth() * 225 / 600,
					Square.getPanelHeight() * 150 / 400, 5, 5);
			gc.fillOval(Square.getPanelWidth() * 275 / 600,
					Square.getPanelHeight() * 375 / 400, 5, 5);
			gc.fillOval(Square.getPanelWidth() * 350 / 600,
					Square.getPanelHeight() * 25 / 400, 5, 5);
			gc.fillOval(Square.getPanelWidth() * 550 / 600,
					Square.getPanelHeight() * 350 / 400, 5, 5);
			gc.fillOval(Square.getPanelWidth() * 550 / 600,
					Square.getPanelHeight() * 40 / 400, 5, 5);
			gc.fillOval(Square.getPanelWidth() * 600 / 600,
					Square.getPanelHeight() * 175 / 400, 5, 5);
			gc.fillOval(Square.getPanelWidth() * 625 / 600,
					Square.getPanelHeight() * 300 / 400, 5, 5);
			gc.fillOval(Square.getPanelWidth() * 425 / 600,
					Square.getPanelHeight() * 300 / 400, 5, 5);
			gc.fillOval(Square.getPanelWidth() * 400 / 600,
					Square.getPanelHeight() * 175 / 400, 5, 5);
			gc.fillOval(Square.getPanelWidth() * 550 / 600,
					Square.getPanelHeight() * 40 / 400, 5, 5);

			// if a box/object is clicked,
			// display the laser beam and the explosion/debris effect
			if (hit) {

				// explosion/debris effect
				if (debrisPos <= 30 && debrisPos >= 0)
					debrisPos += 10;
				else
					debrisPos = -1;

				// display the explosion/debris effect
				gc.setColor(Color.WHITE);
				gc.fillRect(mousePosX + debrisPos, mousePosY + debrisPos, 1, 1);
				gc.fillRect(mousePosX - debrisPos, mousePosY + debrisPos, 1, 1);
				gc.fillRect(mousePosX + debrisPos, mousePosY - debrisPos, 1, 1);
				gc.fillRect(mousePosX - debrisPos, mousePosY - debrisPos, 1, 1);

				// laser beam
				if (laserPosY > mousePosY && laserPosY * .6 > mousePosY)
					laserPosY *= 0.6;
				else {
					// reset laser beam position and colour when it reaches the box/object
					hit = false;
					laserPosY = Square.getPanelHeight() - 25;
					randomColor = (int) (Math.random() * 4);
				}

				// randomize the laser beam colour
				if (randomColor == 0)
					gc.setColor(Color.YELLOW);
				else if (randomColor == 1)
					gc.setColor(Color.CYAN);
				else if (randomColor == 2)
					gc.setColor(Color.GREEN);
				else if (randomColor == 3)
					gc.setColor(Color.PINK);

				// display the laser beam
				gc.fillRect(mousePosX, laserPosY, 1, 25);
			}

			// draw each square in the panel
			for (Square square : squareList) {
				square.paint(gc);
			} // end for
		} // end paintComponent
	} // end class MovingSquarePanel

	protected int mousePosX = 0;
	protected int mousePosY = 0;

	public class Listener implements MouseListener {

		public void mouseEntered(MouseEvent event) {
		}

		public void mouseExited(MouseEvent event) {
		}

		public void mousePressed(MouseEvent event) {
			mousePosX = event.getX();
			mousePosY = event.getY();

			if (!paused) {// do not allow users to click boxes/objects when game is paused
				for (int i = 0; i < squareList.size(); i++) {
					// go through the square list and check if the mouse pointer is inside any
					Square square = squareList.get(i);
					if (square.inside(mousePosX, mousePosY)) {

						//enable the laser beam, explosion/debris effects
						hit = true;
						debrisPos = 0;

						//add/subtract the respective value of the box/object to/from the score total
						if (square.getColor() == squareColors[0]) {
							scoreTotal += 1;
						} else if (square.getColor() == squareColors[1]) {
							scoreTotal += 2;
						} else if (square.getColor() == squareColors[2]) {
							scoreTotal += 3;
						} else if (square.getColor() == squareColors[3]) {
							scoreTotal += 4;
						} else if (square.getColor() == squareColors[4]) {
							scoreTotal -= 1;
						}

						squareList.remove(i); //remove the box/object
						scoreField.setText(scoreTotal + " pts"); //update the score counter
					}
				}
			}
		}

		public void mouseReleased(MouseEvent event) {
		}

		public void mouseClicked(MouseEvent event) {

		}
	}

	public static void main(String args[]) {
		// create an instance of this class and let it run
		new MovingShapes();

	} // end main

} // end class MovingShapes
